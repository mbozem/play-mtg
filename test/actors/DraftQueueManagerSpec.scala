package actors

import akka.actor.{Actor, Props, ActorSystem}
import akka.testkit.{TestProbe, TestActorRef, ImplicitSender, TestKit}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, Matchers, FlatSpecLike}
import org.scalatest.time.{Millis, Span}
import org.scalatest.concurrent.Eventually
import actors.messages._
import actors.messages.DraftQueue
import actors.messages.PackInfo
import actors.messages.DraftInformation
import actors.messages.DraftQueueJoined

class DraftQueueManagerSpec(_system: ActorSystem) extends TestKit(_system)
      with FlatSpecLike
      with ImplicitSender
      with Matchers
      with BeforeAndAfterAll
      with BeforeAndAfter
      with Eventually {

  var dqmRef:TestActorRef[DraftQueueManager] = _
  var dqm: DraftQueueManager = _

  val draftId = "1"
  val player1Id = "1"
  val player2Id = "2"

  val createMsg = (CreateDraftQueueMsg(DraftInformation(8, List(PackInfo("Timespiral")))), player1Id)
  val joinMsg = (JoinDraftQueueMsg(draftId), player2Id)
  val sampleQueue = DraftQueue(draftId, List(player1Id), DraftInformation(8, List(PackInfo("Timespiral"))))
  val queueWithBothPlayers = DraftQueue(sampleQueue.draftId, player2Id :: sampleQueue.players, sampleQueue.draftInformation)

  before {
    dqmRef = TestActorRef(new DraftQueueManager())
    dqm = dqmRef.underlyingActor
  }

  "The DraftQueueManager" should "create draft queues" in {
    dqm.queues.size should be (0)
    dqmRef ! createMsg
    expectMsgClass(classOf[DraftQueueJoined])
    eventually { dqm.queues.size should be (1) }
  }

  it should "not allow players to create a queue if they are already in one" in {
    dqm.queues = sampleQueue :: dqm.queues
    dqm.queues.size should be (1)
    dqm.receive(createMsg)
    dqm.queues.size should be (1)
  }

  it should "allow players to join queues" in {
    dqm.queues = sampleQueue :: dqm.queues
    dqmRef ! joinMsg
    expectMsg(DraftQueueJoined(queueWithBothPlayers))
    eventually { dqm.queues.filter(_.draftId == draftId)(0).players.length should be (2) }
  }

  it should "not allow players to join a queue if they are already in one" in {
    dqm.queues = queueWithBothPlayers :: dqm.queues
    dqm.receive(joinMsg)
    dqm.queues.filter(_.draftId == draftId)(0).players.length should be (2)
  }

  it should "not allow players to join a queue if the queue is already full" in {
    val fullQueue = DraftQueue(sampleQueue.draftId, sampleQueue.players, DraftInformation(1, List()))
    dqm.queues = fullQueue :: dqm.queues
    dqm.receive(joinMsg)
    dqm.queues.filter(_.draftId == draftId)(0).players.length should be (1)
  }

  it should "allow players to leave a queue" in {
    dqm.queues = queueWithBothPlayers :: dqm.queues
    dqmRef ! (LeaveDraftQueueMsg(draftId), player2Id)
    expectMsg(DraftQueueLeft)
    eventually { dqm.queues.filter(_.draftId == draftId)(0).players should not contain player2Id }
  }

  it should "start a queue once its filled with players" in {
    //dqm.queues = DraftQueue(draftId, List(player1Id), DraftInformation(2, List(PackInfo("TimeSpiral")))) :: dqm.queues
    //dqmRef ! joinMsg
    /*
    val player = TestActorRef(Props(new Actor {
      def receive = {
        case _ =>
      }
    }), "player1")
    println(player.path)
    */
  }

  implicit override val patienceConfig = PatienceConfig(timeout = scaled(Span(123, Millis)), interval = scaled(Span(15, Millis)))
  def this() = this(ActorSystem("MySpec"))
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }
}