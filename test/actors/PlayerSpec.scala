package actors

import akka.testkit.{TestActorRef, TestProbe, ImplicitSender, TestKit}
import org.scalatest._
import akka.actor.{ActorRef, Props, ActorSystem}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Span}
import actors.messages._
import helpers.StubWebSocketOut
import actors.messages.IncomingClientMsg
import scala.Some
import actors.messages.DraftInformation
import actors.messages.CreateDraftQueueMsg
import actors.messages.UserStatus._

class PlayerSpec(_system: ActorSystem) extends TestKit(_system)
        with FlatSpecLike
        with ImplicitSender
        with Matchers
        with BeforeAndAfterAll
        with BeforeAndAfter
        with Eventually {

  var probe: TestProbe = _
  var out: StubWebSocketOut = _
  var player: ActorRef = _
  val id = "1"

  before {
    probe = new TestProbe(system)
    out = new StubWebSocketOut
    player = system.actorOf(Props(new Player(probe.ref, id, out, NotInDraft)))
  }
  after { system.stop(player) }

  "A Player" should "on creation send its status to the client" in {
    eventually (timeout(Span(100, Millis))) { out.actual should include ("NOT_IN_DRAFT")}
  }

  it should "forward create draft messages to the queue manager" in {
    val expectedMsg = (CreateDraftQueueMsg(DraftInformation(1, List())), id)

    player ! IncomingClientMsg(Some(expectedMsg._1), None, None, None)

    val actualMsg = probe.fishForMessage() {
      case `expectedMsg` => true
      case _ => false
    }
    actualMsg should equal (expectedMsg)
  }

  it should "forward join draft messages to the queue manager" in {
    val draftId = "1"
    val expectedMsg = (JoinDraftQueueMsg(draftId), id)

    player ! IncomingClientMsg(None, Some(JoinDraftQueueMsg(draftId)), None, None)

    val actualMsg = probe.fishForMessage() {
      case `expectedMsg` => true
      case _ => false
    }

    actualMsg should equal (expectedMsg)
  }

  it should "forward leave draft messages to the queue manager" in {
    val draftId = "1"
    val expectedMsg = (LeaveDraftQueueMsg(draftId), id)

    player ! IncomingClientMsg(None, None, Some(LeaveDraftQueueMsg(draftId)), None)

    val actualMsg = probe.fishForMessage() {
      case `expectedMsg`=> true
      case _ => false
    }

    actualMsg should equal (expectedMsg)
  }

  it should "update its status when it receives a draft joined and left msg" in {
    val playerActorRef = TestActorRef[Player](Props(new Player(probe.ref, id, out, NotInDraft)))
    val playerActor = playerActorRef.underlyingActor

    val draftId = "1337"
    playerActor.receive(DraftQueueJoined(DraftQueue(draftId, List(), DraftInformation(1, List()))))
    out.actual should include (draftId)
    playerActor.receive(DraftQueueLeft)
    out.actual should not include draftId
  }

  it should "send outgoing messages to the client" in {
    val playerActorRef = TestActorRef[Player](Props(new Player(probe.ref, id, out, NotInDraft)))
    val playerActor = playerActorRef.underlyingActor

    playerActor.receive(DraftQueuesMsg(List()))
    out.actual should include (""""msgType":"DraftQueues"""")
  }

  def this() = this(ActorSystem("MySpec"))
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }
}
