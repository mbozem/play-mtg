package actors

import akka.actor.{ActorRef, ActorSystem, Actor, Props}
import akka.testkit.{TestActorRef, TestProbe, TestKit, ImplicitSender}
import org.scalatest._
import scala.collection.immutable.HashMap
import scala.concurrent.duration._
import play.mvc.WebSocket.Out
import helpers.{StubWebSocketOut, NilActor}
import actors.messages._
import actors.messages.WSMessage
import actors.messages.IncomingClientMsg
import scala.Some
import actors.messages.JoinDraftQueueMsg
import actors.messages.DraftQueuesMsg

trait StubPlayerCreator extends PlayerCreator {
  override def newPlayer(playerId: String, out: Out[String], status: String): Actor = {
    new NilActor
  }
}

final class TestPlayerAdmin(watcher: ActorRef) extends PlayerAdmin with StubPlayerCreator {
  players = HashMap[String, ActorRef]("1" -> watcher)

}

class PlayerAdminSpec(_system: ActorSystem) extends TestKit(_system)
        with FlatSpecLike
        with ImplicitSender
        with Matchers
        with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec"))
  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "The PlayerAdmin" should "send outgoing client messages to all connected players" in {
    val probe = new TestProbe(system)
    val playerAdmin = system.actorOf(Props(new TestPlayerAdmin(probe.ref)))
    val msg = DraftQueuesMsg(List())

    playerAdmin ! msg

    val actualMsg = probe.receiveOne(500 millis)
    assert(msg == actualMsg)
  }

  it should "forward incoming WebSocket messages" in {
    val probe = new TestProbe(system)
    val playerAdmin = system.actorOf(Props(new TestPlayerAdmin(probe.ref)))
    val draftId = "137f9342-353f-4c7d-9dc6-4f1f153666e0"
    val jsonMsg = s""" {"joinDraftQueueMsg":{"draftId":"$draftId"}} """
    val expectedMsg = IncomingClientMsg(None, Some(JoinDraftQueueMsg(draftId)), None, None)

    playerAdmin ! WSMessage("1", jsonMsg)

    val actualMsg = probe.receiveOne(500 millis)
    assert(actualMsg == expectedMsg)
  }

  it should "create player actors when a new WebSocket connection is established" in {
    val probe = new TestProbe(system)
    val playerAdmin = TestActorRef(new TestPlayerAdmin(probe.ref)).underlyingActor
    val playerId = "2"
    val out = new StubWebSocketOut

    playerAdmin.players.get(playerId) should be (None)
    playerAdmin.receive(PlayerConnected(playerId, out))
    playerAdmin.players.get(playerId) should not be None
  }
}

