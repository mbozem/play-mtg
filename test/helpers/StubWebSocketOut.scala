package helpers

import play.mvc.WebSocket

class StubWebSocketOut extends WebSocket.Out[String]() {
  var actual: String = null

  def write(node: String) {
    actual = node
  }

  def close() {}
}
