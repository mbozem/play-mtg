var connection;

$(document).ready(function() {
	var wsUrl = $('#ws-url').val();
	console.log(wsUrl);

	connection = new WebSocket(wsUrl);
	connection.onopen = function() {
		console.log("connection established ...");
	}
	connection.onmessage = function(event) {
		console.log("onmessage: " + event);
	}
	connection.onerror = function(error) {
		console.log("onerror: " + error);
	}
	connection.onclose = function() {
		console.log("connection closed ...");
	}
});