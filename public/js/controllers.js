/* Controllers */

angular.module('myApp.controllers', [])
  .controller('LoginCtrl', ['$scope', '$location', 'StatusService', function($scope, $location, StatusService) {
    $scope.username = "";
    $scope.canSubmit = true;
    $scope.logIn = function() {
      $scope.canSubmit = false;
      StatusService.logIn($scope.username).then(function(result) {
        if (result === true) {
          $location.path('/lobby');
        } else {
          alert(result);
          $scope.canSubmit = true;
        }
      });
    };
  }])

  .controller('LobbyCtrl', ['$scope', '$http', '$location', 'MtgEditions', 'StatusService', function($scope, $http, $location, MtgEditions, StatusService) {

    $scope.connectionManager = StatusService;

    $scope.formDisabled = true;
    $scope.draftQueues = StatusService.draftQueues;

    $scope.isPlayerQueue = function(q) {
      return StatusService.getPlayerQueueId() === q.draftId;
    };
    $scope.notInQueue = function() {
      return ! StatusService.playerIsInQueue() ;
    };

    StatusService.getAvailableDE().then(function(result) {
      $scope.formDisabled = false;

      var draftableEditions = [];
      for (var i = result.length - 1; i >= 0; i--) {
        draftableEditions.push({name: result[i].name, id: result[i].id});
      }
      draftableEditions.sort(function(a, b) {
        if(a.name < b.name) return -1;
        if(a.name > b.name) return 1;
        return 0;
      });

      $scope.draftableEditions = draftableEditions;
      
      $scope.draft = {
        maxPlayers: 8,
        packs: [$scope.draftableEditions[0], $scope.draftableEditions[0], $scope.draftableEditions[0]]
      };      
    });


    $scope.startDraftQueue = function() {
     $scope.formDisabled = true;
     StatusService.startDraftQueue($scope.draft);
    };

    $scope.joinQueue = function(q) {
      StatusService.joinQueue(q);
    };
    $scope.leaveQueue = function() {
      StatusService.leaveQueue();
    };  
  }])

  .controller('WaitingInQueueCtrl', ['$scope', 'StatusService', function($scope, StatusService) {

    $scope.playerQueue = StatusService.playerQueue;

    $scope.playersInQueue = function() {
      return StatusService.playerQueue.queue.players.length;
    };
  }])

  .controller('DraftingCtrl', ['$scope', 'DraftingService', function($scope, DraftingService) {
    $scope.availablePicks = DraftingService.availablePicks;
    $scope.pickedCards = [];

    $scope.choosePick = function(card) {
      $scope.pickedCards.push(card);
      DraftingService.choosePick(card);
    };
  }])

  .controller('DeckBuildingCtrl', ['$scope', 'DeckBuildingService', function($scope, DeckBuildingService) {
    $scope.cardsForDeckbuilding = DeckBuildingService.cardsForDeckbuilding;
    $scope.cardsInDeck = DeckBuildingService.cardsInDeck;
    $scope.lands = ['plains', 'island', 'swamp', 'mountain', 'forest'];
    $scope.landsInDeck = DeckBuildingService.landsInDeck;

    $scope.addToDeck = function(card) {     
      var index = DeckBuildingService.cardsForDeckbuilding.cards.indexOf(card);
      if (index > -1) {
          DeckBuildingService.cardsForDeckbuilding.cards.splice(index, 1);
          DeckBuildingService.cardsInDeck.cards.push(card);
      } else {
      }
    };
    $scope.removeFromDeck = function(card) {
      var index = DeckBuildingService.cardsInDeck.cards.indexOf(card);
      if (index > -1) {
          DeckBuildingService.cardsInDeck.cards.splice(index, 1);
          DeckBuildingService.cardsForDeckbuilding.cards.push(card);
      } else {
      }
    };
    $scope.addLand = function(land) {
      DeckBuildingService.landsInDeck[land]++;
    };
    $scope.removeLand = function(land) {
      DeckBuildingService.landsInDeck[land]--;
    };
    $scope.buildDraftDeck = function() {
      DeckBuildingService.buildDraftDeck();
    };
  }])
/*
demo for a modal controller?
  .controller('MyCtrl2', ['$scope', '$modal', function($scope, $modal) {
    $scope.open = function () {
      var modalInstance = $modal.open({
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        resolve: {}
      });

      modalInstance.result.then(function() {
      }, function () {
      });
    };
  }])
*/

  .controller('HeaderController', ['$scope', function($scope) {
    $scope.headerMessage = "Hello from Ctrl";
  }])

  .controller('PlayCtrl', ['$scope', '$rootScope', 'GameObject', 'PlayService', function($scope, $rootScope, GameObject, PlayService) {

    var cardBelongsToEnemy = function(eventTarget) {
      return eventTarget.className.indexOf("opponent-card") > -1;
    };
    var getCardForIndex = function(cardIndex, isEnemyCard) {
      if (isEnemyCard) {
        return GameObject.getEnemyCardForIdx(cardIndex);
      } else {
        return GameObject.ownMatchState.inPlay[cardIndex];
      }
    };
    var getCardIndex = function(eventTarget, isEnemyCard) {
      var w;
      if (isEnemyCard) {
        w = document.getElementById('enemy-cards-in-play-wrapper');
      } else {
        w = document.getElementById('own-top-wrapper');
      }
      
      var cards = w.getElementsByClassName('card-in-play');
      for (var i = 0; i < cards.length; i++) {
        if (eventTarget === cards[i]) {
          return i;
        }   
      } 
    };
    var getHandCardIndex = function(eventTarget) {
      var h = document.getElementById('own-hand-wrapper');
      var cards = h.getElementsByClassName('own-card-in-hand');
      for (var i = 0; i < cards.length; i++) {
        if (eventTarget === cards[i]) {
          return i;
        }   
      }
      return -1;   
    };

    var position = {};
    interact('.own-card-in-hand')
    .draggable(true)
    .on('dragstart', function (event) {
      position.x = 0;
      position.y = 0;
    })
    .on('dragmove', function (event) {
      position.x += event.dx;
      position.y += event.dy;

      event.target.dataset.x = position.x;
      event.target.dataset.y = position.y;
      event.target.style.webkitTransform = event.target.style.transform = 'translate(' + position.x + 'px, ' + position.y + 'px)';
    });    

    interact('#own-cards-in-play-wrapper').dropzone({
        accept: '.own-card-in-hand',
        overlap: 0.40,
        ondropactivate: function (event) {},
        ondragenter: function (event) {},
        ondragleave: function (event) {},
        ondrop: function (event) {
          var cardIndex = getHandCardIndex(event.relatedTarget);
          if (cardIndex >= 0) {
            event.relatedTarget.setAttribute('data-dropped', 'true');
            $scope.playCardAtIndex(cardIndex);
          }
        },
        ondropdeactivate: function (event) {
          //position = {};//this shouldnt be done here...
          if (!event.relatedTarget.getAttribute('data-dropped')) {
            event.relatedTarget.style.webkitTransform = event.relatedTarget.style.transform = 'none';
          }
        }
    });    
    
    interact('.draggable')
    .draggable({
        onstart: function (event) {
          if (event.button !== 0) return;

          var isEnemyCard = cardBelongsToEnemy(event.target);
          var cardIndex = getCardIndex(event.target, isEnemyCard);
          $scope.updateZIdx(cardIndex, isEnemyCard);

          //while dragging the card, don't animate it
          event.target.setAttribute('data-transition', event.target.style.transitionProperty);
          event.target.style.transitionProperty = "none";
        },
        onmove: function (event) {
          if (event.button !== 0) return;

            var target = event.target,
                x = (parseFloat(target.style.left) || 0) + event.dx;

            var y;
            if (cardBelongsToEnemy(event.target)) {
              y = (parseFloat(target.style.bottom) || 0) - event.dy;
              target.style.bottom = y + 'px';
            } else {
              y = (parseFloat(target.style.top) || 0) + event.dy;
              target.style.top = y + 'px';  
            }
            target.style.left = x + 'px';
            
        },
        onend: function (event) {
          if (event.button !== 0) return;

          //start animating it again
          event.target.style.transitionProperty = event.target.getAttribute('data-transition'); //"top, left";

          //tell the ctrl that a card moved
          var isEnemyCard = cardBelongsToEnemy(event.target);
          var cardIndex = getCardIndex(event.target, isEnemyCard);
          var x = event.target.style.left;
          var y;
          if (isEnemyCard) {
            y = event.target.style.bottom;
          } else {
            y = event.target.style.top;
          }
        
          $scope.cardMoved(cardIndex, isEnemyCard, parseFloat(x.substring(0, x.length-2)), parseFloat(y.substring(0, y.length-2)));

        }
    })
    .inertia(false)
    .restrict({
        drag: "parent",
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    });    

    $scope.ownMatchState = GameObject.ownMatchState;
    $scope.otherPlayersState = GameObject.otherPlayersState;

    $scope.showDeck = function() {
      return $scope.ownMatchState.deckSize > 0;
    };

    $scope.drawCard = function() {
      PlayService.drawCards(1);
    };
    $scope.playCard = function(card) {
      PlayService.playCard(card);
    };
    $scope.playCardAtIndex = function(cardIndex) {
      var card = GameObject.getHandCardForIndex(cardIndex);
      PlayService.playCard(card);
    };
    $scope.tapCard = function($event, card, isEnemyCard) {
      if ($event.which === 2) {
        PlayService.tapCard(card, isEnemyCard);
      }
    };
    $scope.changeLife = function(amount) {
      PlayService.changeLife(amount);
    };
    $scope.declareResult = function(resStatus) {
      PlayService.declareResult(resStatus);
    };
    $scope.cardMoved = function(idx, isEnemyCard, x, y) {
      var card = getCardForIndex(idx, isEnemyCard);
      var cardZIndex = card.position.zIndex;
      PlayService.moveCard(idx, isEnemyCard, x, y, cardZIndex);
    };
    $scope.updateZIdx = function(cardIndex, isEnemyCard) {
      //1. calculate currently highest z-index
      //2. update the cards z-index if necessary
      var highestZIdx = 0;
      if (!isEnemyCard) {
        for (var i = 0; i < GameObject.ownMatchState.inPlay.length; i++) {
          if (GameObject.ownMatchState.inPlay[i].position.zIndex > highestZIdx) {
            highestZIdx = GameObject.ownMatchState.inPlay[i].position.zIndex;
          }
        }
      } else {
        for (var id in GameObject.otherPlayersState)  {
          var cardsInPlay = GameObject.otherPlayersState[id].inPlay;
          for (var j = 0; j < cardsInPlay.length; j++) {
            if (cardsInPlay[j].position.zIndex > highestZIdx) {
              highestZIdx = cardsInPlay[j].position.zIndex;
            }            
          }
        }
      }

      var card = getCardForIndex(cardIndex, isEnemyCard);
      if (!card.position.zIndex || highestZIdx === 0 || card.position.zIndex < highestZIdx) {
        $rootScope.$apply(function() {
          card.position.zIndex = highestZIdx + 1;
        }); 
      }
      //return card.position.zIndex;
    };
    $scope.cardStyle = function(card, isEnemyCard) {
      var zIdx;
      if (card.position.zIndex !== undefined) {
        zIdx = card.position.zIndex;
      } else {
        zIdx = 0;
      }

      if (isEnemyCard) {
        return {left: card.position.x + 'px', bottom: card.position.y +'px', 'z-index': zIdx};
      } else {
        return {left: card.position.x + 'px', top: card.position.y +'px', 'z-index': zIdx};
      }
    };
    $scope.menuOptions = [
        ['Buy', function ($itemScope) {
            console.log($itemScope.card);
        }],
        null,
        ['Sell', function ($itemScope) {
            
        }]
    ];    
  }])

  .controller('ModalInstanceCtrl', ['$scope', '$modalInstance', function($scope, $modalInstance) {
    $scope.credentials = {username: '', password: ''};

    $scope.ok = function () {
      $modalInstance.close();
    }; 

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.attemptLogin = function() {
    };
  }]);
