// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers',
  'ui.bootstrap'
]).
run(function (DraftingService, DeckBuildingService, PlayService) {

}).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when(
    '/testGame', 
    {
      templateUrl: 'assets/partials/play.html',
      controller: 'PlayCtrl'
    }
  );  
  $routeProvider.when(
    '/lobby', 
    {
      templateUrl: 'assets/partials/lobby.html', 
      controller: 'LobbyCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.authenticate();
        }
      }
    }
  );
  $routeProvider.when(
    '/login', 
    {
      templateUrl: 'assets/partials/login.html', 
      controller: 'LoginCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.notLoggedIn();
        }
      }
    }
  );
  $routeProvider.when(
    '/play', 
    {
      resolve: {
        auth: function(AuthService) {
          return AuthService.gameView();
        }
      }
    }
  );  
  $routeProvider.when(
    '/waiting-in-queue', 
    {
      templateUrl: 'assets/partials/waitingInQueue.html', 
      controller: 'WaitingInQueueCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.forStatus('WAITING_IN_QUEUE');
        }
      }
    }
  );  
  $routeProvider.when(
    '/drafting', 
    {
      templateUrl: 'assets/partials/drafting.html', 
      controller: 'DraftingCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.forStatus('DRAFTING');
        }
      }
    }
  );
  $routeProvider.when(
    '/deckbuilding', 
    {
      templateUrl: 'assets/partials/deckbuilding.html', 
      controller: 'DeckBuildingCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.forStatus('DECKBUILDING');
        }
      }
    }
  );
  $routeProvider.when(
    '/playing', 
    {
      templateUrl: 'assets/partials/play.html', 
      controller: 'PlayCtrl',
      resolve: {
        auth: function(AuthService) {
          return AuthService.forStatus('PLAYING_MATCH');
        }
      }
    }
  );
  //$routeProvider.when('/play', {templateUrl: 'assets/partials/play.html', controller: 'PlayCtrl'});
  $routeProvider.otherwise({redirectTo: '/lobby'});
}])

.directive('ngContextMenu', function ($parse) {
    var renderContextMenu = function ($scope, event, options) {
        if (!$) { var $ = angular.element; }
        $(event.currentTarget).addClass('context');
        var $contextMenu = $('<div>');
        $contextMenu.addClass('dropdown clearfix');
        var $ul = $('<ul>');
        $ul.addClass('dropdown-menu');
        $ul.attr({ 'role': 'menu' });
        $ul.css({
            display: 'block',
            position: 'absolute',
            left: event.pageX + 'px',
            top: event.pageY + 'px'
        });
        angular.forEach(options, function (item, i) {
            var $li = $('<li>');
            if (item === null) {
                $li.addClass('divider');
            } else {
                $a = $('<span>');
                $a.attr({ tabindex: '-1' }); //href: ''
                $a.text(item[0]);
                $li.append($a);
                $li.on('click', function () {
                    $scope.$apply(function() {
                        item[1].call($scope, $scope);
                    });
                });
            }
            $ul.append($li);
        });
        $contextMenu.append($ul);
        $contextMenu.css({
            width: '100%',
            height: '100%',
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: 9999
        });
        $(document).find('body').append($contextMenu);
        $contextMenu.on("click", function (e) {
            $(event.currentTarget).removeClass('context');
            $contextMenu.remove();
        }).on('contextmenu', function (event) {
            $(event.currentTarget).removeClass('context');
            event.preventDefault();
            $contextMenu.remove();
        });
    };
    return function ($scope, element, attrs) {
        element.on('contextmenu', function (event) {
            $scope.$apply(function () {
                event.preventDefault();
                var options = $scope.$eval(attrs.ngContextMenu);
                if (options instanceof Array) {
                    renderContextMenu($scope, event, options);
                } else {
                    throw '"' + attrs.ngContextMenu + '" not an array';                    
                }
            });
        });
    };
});