/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]).
  directive('enter', [function() {
    return function(scope, element, attrs) {
        element.bind("mouseenter", function() {
          console.log("mouseenter attrs.enter: " + attrs.enter);
            scope.$apply(attrs.enter);
        });
    };
  }]);