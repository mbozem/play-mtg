/* Services */

angular.module('myApp.services', []).
  value('version', '0.1')

  .factory('WS', ['$rootScope', function($rootScope) {
    var wsUrl = angular.element(document.querySelector('#ws-url')).val();
    var ws = new WebSocket(wsUrl);
    ws.onmessage = function(event) {
      var message = JSON.parse(event.data);
      console.log("msg received", message);
      var processed = 0;
      $rootScope.$apply(function() {
        for (var i = Service.messageObservers.length - 1; i >= 0; i--) {
          var currObs = Service.messageObservers[i];
          if (currObs(message)) {
            processed++;
          }
        }
        if (processed === 0) {
          console.log("unhandled msg");
        }
      });
    }; 

    var Service = {
      ws: ws,
      messageObservers: [],
      addObserver: function(obs) {
        this.messageObservers.push(obs);
      },
      send: function(payload) {
        this.ws.send(JSON.stringify(payload));
      }
    };
    return Service;
  }])

  .factory('StatusService', ['$q', '$location', 'WS', function($q, $location, WS) {
    WS.addObserver(function(message) {
      switch(message.msgType) {
        case 'LoginMsg':
          Service.playerLoggedIn();
          return true;
        case 'InvalidLoginMsg':
          Service.invalidLogin(message.reason);
          return true;
        case 'ConnectedUsers':
          Service.setUsersOnline(message.users, message.notLoggedInCount);
          return true;
        case 'DraftQueues':
          Service.setQueues(message.queues);       
          return true;
        case 'UserStatus':
          Service.updateStatus(message);     
          return true;
        case 'AvailableDraftEditions':
          Service.setAvailableDE(message.editions);
          return true;
      }      
    });

    var Service = {};
    Service.users = {users: [], notLoggedInCount: 0, usersOnline: 0};
    Service.draftQueues = {queues: []};
    Service.playerQueue = {id: undefined, queue: undefined};
    Service.playerStatus = {status: undefined};
    Service.loggedIn = {isLoggedIn: false, waitingForLogin: undefined};
    Service.availableDraftEditions = [];

    Service.playerLoggedIn = function() {
      this.loggedIn.isLoggedIn = true;
      if (this.loggedIn.waitingForLogin) {
        this.loggedIn.waitingForLogin.resolve(true);
        this.loggedIn.waitingForLogin = undefined;
      }
    };
    Service.invalidLogin = function(reason) {
      this.loggedIn.isLoggedIn = false;
      if (this.loggedIn.waitingForLogin) {
        this.loggedIn.waitingForLogin.resolve(reason);
        this.loggedIn.waitingForLogin = undefined;
      }
    };
    Service.setUsersOnline = function(users, notLoggedInCount) {
      this.users.users = users;
      this.users.notLoggedInCount = notLoggedInCount;
      this.users.usersOnline = notLoggedInCount + users.length;
    };
    Service.setQueues = function(queues) {
      this.draftQueues.queues = queues;
      for (var i = 0; i <= this.draftQueues.queues.length - 1; i++) {
        var currQueue = this.draftQueues.queues[i];
        if (currQueue.draftId === this.playerQueue.id) {
          this.playerQueue.queue = currQueue;
        }
      }      
    };
    Service.updateStatus = function(message) {
      var oldStatus = this.playerStatus.status;
      this.playerStatus.status = message.status;
      if (message.status !== 'NOT_IN_DRAFT') {
        this.playerQueue.id = message.draftId;              
      } else {
        this.playerQueue.id = undefined;
      } 
      this.updateGameWindow(oldStatus); 
    };
    Service.updateGameWindow = function(oldStatus) {
      if (this.playerStatus.status !== 'NOT_IN_DRAFT') {
        $location.path('/play');
      }
    };
    Service.isLoggedIn = function() {
      return this.loggedIn.isLoggedIn;
    };
    Service.startDraftQueue = function(draftObject) {
      var payload = {message: {draftInformation: draftObject, theType: 'CreateDraftQueueMsg'}};
      WS.send(payload);
    };
    Service.joinQueue = function(queue) {
      var msg = {message: {draftId: queue.draftId, theType: 'JoinDraftQueueMsg'}};
      WS.send(msg);
    };
    Service.leaveQueue = function() {
      var msg = {message: {draftId: "", theType: 'LeaveDraftQueueMsg'}};
      WS.send(msg);
    };
    Service.getPlayerQueueId = function() {
      return this.playerQueue.id;
    };

    Service.playerIsInQueue = function() {
      return this.playerStatus.status !== 'NOT_IN_DRAFT';
    };
    Service.logIn = function(username) {
      this.loggedIn.waitingForLogin = $q.defer();
      var payload = {message: {username: username, theType: 'LoginMessage'}};

      WS.send(payload);
      return this.loggedIn.waitingForLogin.promise;
    };
    Service.setAvailableDE = function(editions) {
      while(this.availableDraftEditions.length > 0) {
          this.availableDraftEditions.pop();
      }      
      for (var i = 0; i < editions.length; i++) {
        this.availableDraftEditions.push(editions[i]);
      }
      while(waitingForDE.length > 0) {
        var curr = waitingForDE.pop();
        curr.resolve(this.availableDraftEditions);
      }
    };

    var waitingForDE = [];
    Service.getAvailableDE = function() {
      var deferred = $q.defer();
      if (this.availableDraftEditions.length === 0) {
        waitingForDE.push(deferred);
      } else {
        deferred.resolve(this.availableDraftEditions);
      }      
      return deferred.promise;
    };

    return Service;
  }])

  .factory('DraftingService', ['WS', function(WS) {

    WS.addObserver(function(message) {
      switch(message.msgType) {
        case 'DraftPicks':
          Service.availablePicks.cards = message.picks;
          return true;
      }      
    }); 

    var Service = {};
    Service.availablePicks = {cards: []};    
    Service.choosePick = function(card) {
      this.availablePicks.cards = [];
      var msg = {message: {card: card, theType: 'ChosenPick'}};
      WS.send(msg);
    };

    return Service;
  }])

  .factory('DeckBuildingService', ['WS', function(WS) {

    WS.addObserver(function(message) {
      switch(message.msgType) {
        case 'BuildDeck':
          Service.cardsForDeckbuilding.cards = message.cards;
          return true;
      }      
    });

    var Service = {};
    Service.cardsForDeckbuilding = {cards: []};
    Service.cardsInDeck = {cards: []};
    Service.landsInDeck = {plains: 0, island: 0, swamp: 0, mountain: 0, forest: 0};

    Service.buildDraftDeck = function() {
      var payload = {message: {cards: this.cardsInDeck.cards, lands: this.landsInDeck, theType: 'BuiltDraftDeck'}};
      WS.send(payload);
    };

    return Service;
  }])

  .factory('PlayService', ['$rootScope', '$q', '$location', 'WS', 'GameObject', function($rootScope, $q, $location, WS, GameObject) {
    
    WS.addObserver(function(message) {
      switch(message.msgType) {
        case 'MatchObject':
          GameObject.setMatchObject(message);
          return true;
        case 'NewMatchStartingMsg':
          GameObject.resetMatchObject();
          return true;
      }      
    });

    var Service = {};

    Service.drawCards = function(amount) {
      var payload = {message: {amount: amount, theType: 'DrawCardsMessage'}};
      WS.send(payload);
    };
    Service.playCard = function(card) {
      var payload = {message: {card: card, theType: 'PlayCardMessage'}};
      WS.send(payload);
    };
    Service.tapCard = function(card, isEnemyCard) {
      var payload = {message: {card_id: card.id, is_enemy_card: isEnemyCard, theType: 'TapCardMessage'}};
      WS.send(payload);
    };
    Service.changeLife = function(amount) {
      var payload = {message: {amount: amount, theType: 'ChangeLifeMessage'}};
      WS.send(payload);
    };
    Service.declareResult = function(resStatus) {
      var payload = {message: {result: resStatus, theType: 'PlayerDeclaredResult'}};
      WS.send(payload);
    };
    Service.moveCard = function(idx, isEnemyCard, x, y, newZIdx) {
      var card;
      if (isEnemyCard) {
        card = GameObject.getEnemyCardForIdx(idx);
      } else {  
        card = GameObject.ownMatchState.inPlay[idx];
      }

      var payload = {message: {card_id: card.id, is_enemy_card: isEnemyCard, x: x, y: y, zIndex: newZIdx, theType: 'CardMovedMessage'}};
      WS.send(payload);
    };

    return Service;
  }])

  .factory('MtgEditions', ['$http', '$q', function($http, $q) {

    var Service = {draftAble: undefined};
    Service.getData = function() {
      var deferred = $q.defer();

      if (this.draftAble) {
        deferred.resolve(this.draftAble);
      } else {
        $http({method:"GET", url:"https://api.deckbrew.com/mtg/sets"}).success(function(result) {
          var draftAble = result.filter(function(elem) {
            return elem.type === "core" || elem.type === "expansion";
          });
          this.draftAble = draftAble;
          deferred.resolve(draftAble);
        });
      }

      return deferred.promise;
    };
    return Service;
  }])

  .factory('GameObject', ['$http', '$rootScope', function($http, $rootScope) {
    var Service = {};

    Service.ownMatchState = {deckSize: 0, exiled: [], graveyard: [], hand: [], inPlay: []};
    Service.otherPlayersState = {};

    Service.setMatchObject = function(matchObject) {
      this.ownMatchState.deckSize = matchObject.ownMatchState.deckSize;
      this.ownMatchState.hand = matchObject.ownMatchState.hand;
      this.ownMatchState.graveyard = matchObject.ownMatchState.graveyard;
      this.ownMatchState.exiled = matchObject.ownMatchState.exiled;
      
      for (var j = 0; j < matchObject.ownMatchState.inPlay.length; j++) {
        var currNew = matchObject.ownMatchState.inPlay[j];

        var isAlreadyInPlay = false;
        var currOld;
        for (var k = 0; k < this.ownMatchState.inPlay.length; k++) {
          currOld = this.ownMatchState.inPlay[k];
          if (currOld.id === currNew.id) {
            isAlreadyInPlay = true;
            break;
          }          
        }
        if (!isAlreadyInPlay) {
          this.ownMatchState.inPlay.push(currNew);          
        } else {
          currOld.position.x = currNew.position.x;
          currOld.position.y = currNew.position.y;
          currOld.position.zIndex = currNew.position.zIndex;
          currOld.position.isTapped = currNew.position.isTapped;
        }
      }      

      this.ownMatchState.life = matchObject.ownMatchState.life;

      for (var i = matchObject.otherPlayersState.length - 1; i >= 0; i--) {
        var otherPlayerState = matchObject.otherPlayersState[i];

        if (!this.otherPlayersState.hasOwnProperty(otherPlayerState.playerId)) {
          this.otherPlayersState[otherPlayerState.playerId] = {inPlay: []};
        }
        var otherPlayer = this.otherPlayersState[otherPlayerState.playerId];
        otherPlayer.deckSize = otherPlayerState.deckSize;
        otherPlayer.exiled = otherPlayerState.exiled;
        otherPlayer.graveyard = otherPlayerState.graveyard;
        otherPlayer.handSize = otherPlayerState.handSize;        
        otherPlayer.life = otherPlayerState.life;

        //otherPlayer.inPlay = otherPlayerState.inPlay;
        for (var l = 0; l < otherPlayerState.inPlay.length; l++) {
          var currCard = otherPlayerState.inPlay[l];
          var existingCard = otherPlayer.inPlay.find(function(el, idx, array) {
            return el.id === currCard.id;
          });
          if (existingCard)  {
            existingCard.position.x = currCard.position.x;
            existingCard.position.y = currCard.position.y;
            existingCard.position.zIndex = currCard.position.zIndex;
            existingCard.position.isTapped = currCard.position.isTapped;
          } else {
            otherPlayer.inPlay.push(currCard);
          }
        }
      }
    };
    Service.resetMatchObject = function() {
      this.ownMatchState = {deckSize: 0, exiled: [], graveyard: [], hand: [], inPlay: []};
      this.otherPlayersState = {};
    };
    Service.getEnemyCardForIdx = function(idx){
      if (Object.keys(this.otherPlayersState).length > 1) {
        throw "This implementation doesn't work if there are multiple opponents!";
      }

      for (var id in this.otherPlayersState)  {
        var cardsInPlay = this.otherPlayersState[id].inPlay;
        return cardsInPlay[idx];
      }
    };
    Service.getHandCardForIndex = function(idx) {
      return this.ownMatchState.hand[idx];
    };

    return Service;
  }])

  .factory('AuthService', ['$q', '$location', 'StatusService', function($q, $location, StatusService) {
    return {
      authenticate: function() {
        if (StatusService.isLoggedIn()) {
          return true;
        } else {
          $location.path('/login');
          return $q.reject('Not Logged In');
        }
      },
      notLoggedIn: function() {
        if (StatusService.isLoggedIn()) { 
          $location.path('/lobby');
          return $q.reject('Already logged in');
        } else {
          return true;
        }
      },
      gameView: function() {
        switch(StatusService.playerStatus.status) {
          case 'WAITING_IN_QUEUE':
            $location.path('/waiting-in-queue');
            return true;
          case 'DRAFTING':
            $location.path('/drafting');
            return true;
          case 'PLAYING_MATCH':
            $location.path('/playing');
            return true;
          case 'DECKBUILDING':
            $location.path('/deckbuilding');
            return true;            
          default:
            $location.path('/lobby');
            return $q.reject('Not in a draft');
        }
      },
      forStatus: function(status) {
        if (StatusService.playerStatus.status === status) {
          return true;
        } else {
          $location.path('/lobby');
        }
      }
    };
  }]);