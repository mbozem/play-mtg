name := "play-mtg"

scalaVersion := "2.11.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

version := "1.0-SNAPSHOT"

javaOptions ++= Seq("-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled", "-XX:+UseConcMarkSweepGC")

resolvers += Resolver.url("Sonatype OSS Releases", url("https://oss.sonatype.org/content/repositories/releases"))(Resolver.ivyStylePatterns)


libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  ws,
  "com.google.code.gson" % "gson" % "2.2",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.3",
  "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
  "org.julienrf" %% "play-json-variants" % "0.2"
)     

//"net.liftweb" %% "lift-json" % "2.6-SNAPSHOT",
