package models

import actors.messages.{LandList, PackInfo}
import play.api.Play
import play.api.Play.current
import scala.util.Random
import play.api.libs.json._
import play.api.libs.functional.syntax._
import actors.messages.IncomingReads._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import actors.ListHelper

object EditionManager {
  def initialize = {}

  val valid: List[PackInfo] = 
  (for(file <- Play.getFile("conf/editions").listFiles()) yield {
    val booster = Converter.convert(PackInfo(name = "", id = file.getName))
    if (booster.isDefined) Some(booster.get._1)
    else None
  }).filter(_.isDefined).map(_.get).toList
  println(s"There are ${valid.size} valid editions to draft")  
}

object Converter {
  /* Type Declarations */
  type Rarity = String
  type Booster = List[MagicCard]
  type RarityChooser = List[Rarity] => Option[Rarity]
  type CardChoser = (Rarity, List[MagicCard], List[MagicCard]) => ChosenCardResult

  sealed trait ChosenCardResult
  case object CardNotFound extends ChosenCardResult
  case object CardRejected extends ChosenCardResult
  case class CardChosen(card: MagicCard) extends ChosenCardResult
  /* End Type Declarations */

  /* CardChoosers */
  /*
    - Finde Karte für Rarity
    -- Wenn rarity eine von den folgenden ist, dann finde einfach eine zufällige karte mit rarity=x:
    --- Set("rare", "uncommon", "common", "mythic rare")
    -- Ist es "land", adde Basic Land
    -- Ist es eine von den folgenden, dann skippe sie: Set("marketing", "checklist")
    -- ist es "timeshifted purple", dann suche eine zufällige aus einer anderen datei
    -- Ist es eine von: "timeshifted common", "timeshifted uncommon", "timeshifted rare":
    --- finde eine  mit der rarity (common,uncommon oder rare), bei der timeshifted=true
    -- ist es "double faced" finde karte mit layout="double faced" mit mana kosten vorhanden 
  */
  private val simpleRarities = Set("rare", "uncommon", "common", "mythic rare")
  private val simpleCardChooser: CardChoser = (rarity, available, used) => 
    if (simpleRarities.contains(rarity.toLowerCase)) {
      search(available, used, _.rarity.toLowerCase == rarity.toLowerCase)
    } else CardNotFound

  private val timeshiftedPurple: MagicSet = SetLoader.setForId("TSB").get
  private val timeshiftedPurpleChooser: CardChoser = (rarity, available, used) =>
    if (rarity != "timeshifted purple") CardNotFound
    else CardChosen(timeshiftedPurple.cards(Random.nextInt(timeshiftedPurple.cards.size))) 

  private val timeshiftedRarities = Set("timeshifted common", "timeshifted uncommon", "timeshifted rare")
  private val timeshiftedChooser: CardChoser = (rarity, available, used) =>
    if (!timeshiftedRarities.contains(rarity)) CardNotFound
    else {
      val splitRarity = rarity.split(' ')(1)
      search(available, used, c => c.rarity.toLowerCase == splitRarity && c.timeshifted.isDefined && c.timeshifted.get)
    }

  private val landChooser: CardChoser = (rarity, available, _) =>
    if (rarity == "land") {
      val available = List("plains", "island", "swamp", "mountain", "forest")
      available(Random.nextInt(available.size)) match {
        case "plains" => CardChosen(Magic.getLands(LandList(plains = 1))(0))
        case "island" => CardChosen(Magic.getLands(LandList(island = 1))(0))
        case "swamp" => CardChosen(Magic.getLands(LandList(swamp = 1))(0))
        case "mountain" => CardChosen(Magic.getLands(LandList(mountain = 1))(0))
        case _ => CardChosen(Magic.getLands(LandList(forest = 1))(0))
      }
      //search(available, List(), c => c.rarity.toLowerCase == "basic land")
    } else CardNotFound

  private val rejectedRarities = Set("marketing", "checklist")
  private val cardRejecter: CardChoser = (rarity, _, _) =>
    if (rejectedRarities.contains(rarity)) CardRejected else CardNotFound

  private val cardChoosers: List[CardChoser] = List(simpleCardChooser, landChooser, cardRejecter, timeshiftedPurpleChooser, timeshiftedChooser)
  /* End CardChoosers */

  /* Rarity Choosers */
  private val planarChaosRC: RarityChooser = {
    rarities =>
      val valid = List("timeshifted rare","timeshifted uncommon")
      if (rarities.sorted != valid.sorted) None
      else {
        val rand = Random.nextInt(4) //see http://archive.wizards.com/Magic/magazine/article.aspx?x=mtgcom/daily/mr261
        if (rand == 0) Some(valid(0)) else Some(valid(1))
      }
  }

  private val futureSightRC: RarityChooser = {
  rarities =>
    val valid = List(List("rare","timeshifted rare"), List("uncommon","timeshifted uncommon"), List("common","timeshifted common"))
    if (!valid.find(_.sorted == rarities.sorted).isDefined) None
    else {
      val rand = Random.nextInt(1) //citation needed
      if (rand == 0) Some(rarities(0)) else Some(rarities(1))
    }    
  }

  private val mythicRC: RarityChooser = {
    rarities =>
      val valid = List("rare","mythic rare")
      if (rarities.sorted != valid.sorted) None
      else {
        val rand = Random.nextInt(8) //see http://archive.wizards.com/Magic/magazine/article.aspx?x=mtgcom/daily/20080602
        if (rand == 0) Some(valid(1)) else Some(valid(0))
      }
  }


  private val rarityChoosers: List[RarityChooser] = List(mythicRC, planarChaosRC, futureSightRC)

  /* End Rarity Choosers */ 

  /*
    BoosterConversion:
    - gehe booster.value der reihe nach durch
    - bestimme die rarity der aktuellen karte und finde zufällige karte zu dieser rarity
  */
  def convert(pack: PackInfo): Option[(PackInfo, Booster)] = {
    SetLoader.setForId(pack.id).flatMap { set =>
      if (!set.booster.isDefined) {
        println(s"Error converting $pack since there is no booster defined for this set")
        None
      } else {
        val res = set.booster.get.value.foldLeft(List[ChosenCardResult]()) { case (booster, jsVal) => 
          val chosenRarity = getRarity(jsVal)
          if (chosenRarity.isDefined) {
            getCard(Random.shuffle(set.cards), chosenRarity.get, takeCards(booster)) :: booster
          } else {
            CardNotFound :: booster
          }     
        }

        val booster = validateDeck(res)
        if (booster.isDefined) Some((PackInfo(name=set.name, id=set.code), booster.get))
        else {
          println(s"convert failed for ${set.code}")
          None
        }
      }
    }
  }

  private def validateDeck(chosen: List[ChosenCardResult]): Option[Booster] = {
    def go(remaining: List[ChosenCardResult], booster: Booster = List()): Option[Booster] = {
      remaining match {
        case r :: rs => 
          r match {
            case CardRejected => go(rs, booster)
            case CardChosen(card) => go(rs, card :: booster)
            case CardNotFound => None
          }
        case Nil => Some(booster)
      }      
    }
    go(chosen)
  }

  private def takeCards(chosen: List[ChosenCardResult]): Booster = {
    def go(remaining: List[ChosenCardResult], booster: Booster = List()): Booster = {
      remaining match {
        case r :: rs => 
          r match {
            case CardChosen(card) => go(rs, card :: booster)
            case _ => go(rs, booster)
          }        
        case Nil => booster
      }      
    }
    go(chosen)
  }

  /*
    - bestimme die rarity der aktuellen karte:
    -- ist booster.value String dann fertig
    -- ist es liste von strings dann choose eine abhängig von individuellen wahrscheinlichkeiten
  */
  private def getRarity(currVal: JsValue): Option[Rarity] = {
    val asString = currVal.asOpt[Rarity]
    val asList = currVal.asOpt[List[Rarity]]

    if (asString.isDefined) asString
    else if (asList.isDefined) {
      def go(remaining: List[RarityChooser]): Option[Rarity] =  {
        remaining match {
          case c :: cs => 
            val res = c(asList.get)
            if (res.isDefined) res else go(cs)
          case Nil => println(s"getRarity failed for ${asList.get}"); None
        }        
      }
      go(rarityChoosers)
    } else {
      println(s"getRarity failed for $currVal")
      None 
    }
  }

  /*
    - finde karte zu rarity, die noch nicht im booster ist: 
  */
  private def getCard(available: List[MagicCard], rarity: Rarity, booster: Booster): ChosenCardResult = {
    def go(choosers: List[CardChoser]): ChosenCardResult = {
      choosers match {
        case c :: cs => 
          c(rarity, available, booster) match {
            case CardNotFound => go(cs)
            case other => other
          }
        case Nil => 
          println(s"getCard failed for rarity=$rarity")
          CardNotFound
      }
      
    }
    go(cardChoosers)
  }

  private def search(available: List[MagicCard], used: List[MagicCard], p: MagicCard => Boolean): ChosenCardResult = {
    available match {
      case a :: as => 
        if (p(a) && !used.contains(a)) CardChosen(a)
        else search(as, used, p)
      case Nil => 
        println(s"search failed for $used")
        CardNotFound
    }
  }  
}