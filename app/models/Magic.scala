package models

import actors.MatchActor.Deck
import actors.messages.{LandList, PackInfo}
import play.api.Play
import play.api.Play.current
import scala.util.Random
import play.api.libs.json._
import play.api.libs.functional.syntax._
import actors.messages.IncomingReads._
import play.api.Play.current
import play.api.libs.ws._
import play.api.libs.ws.ning.NingAsyncHttpClientConfigBuilder
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Magic {

  private def cardEndpoint(id: String) = s"http://mtgjson.com/json/$id.json";

  lazy val ravnicaString = scala.io.Source.fromURL(Play.resource("RAV.json").get).getLines().mkString

  val ravnica1 = Json.parse(ravnicaString).validate[MagicSet]
  val ravnica: MagicSet = ravnica1.asInstanceOf[JsSuccess[MagicSet]].get

  lazy val ravnicaRares = ravnica.cards.filter(_.rarity == "Rare")
  lazy val ravnicaUncommon = ravnica.cards.filter(_.rarity == "Uncommon")
  lazy val ravnicaCommon = ravnica.cards.filter(_.rarity == "Common")
  //lazy val ravnicaLands = ravnica.cards.filter(_.`type`.contains("Basic Land"))
  private var ravnicaLands: Map[String, List[MagicCard]] = _
  private def initLands(): Unit = {
    ravnicaLands = ravnica.cards.filter(_.`type`.contains("Basic Land")).groupBy { card =>
      card.`type`.substring(card.`type`.lastIndexOf(' ')+1).toLowerCase
    }
  }
  initLands()

  def makeRandomDeck(): (Deck, LandList) = {
    val cards = for (
      i <- 0 until 20
    ) yield ravnicaCommon(Random.nextInt(ravnicaRares.size))
    (cards.toList, LandList( 4, 4, 4, 4, 4))
  }

  def getLands(lands: LandList): List[MagicCard] = {
    def take(amount: Int, landType: String): List[MagicCard] = {
      Random.shuffle(ravnicaLands(landType)).take(amount)
    }
    take(lands.plains, "plains") ++
    take(lands.island, "island") ++
    take(lands.swamp, "swamp") ++
    take(lands.mountain, "mountain") ++
    take(lands.forest, "forest")
  }
}

object SetLoader {
  def setForId(id: String): Option[MagicSet] = {
    readJsonFile(id).flatMap { json =>
      val res = Json.parse(json).validate[MagicSet]
      res match {
        case s: JsSuccess[MagicSet] => Some(s.get)
        case e: JsError => 
          println(s"SetLoader failed with Errors for id=$id " + JsError.toFlatJson(e).toString().take(100))
          None
      }
    }    
  }
  
  private def readJsonFile(id: String): Option[String] = {
    val inEditions = "editions/"+id
    val res = Play.resource(if (inEditions.contains(".json")) inEditions else s"$inEditions.json")
    if (res.isDefined) Some(scala.io.Source.fromURL(res.get)("UTF-8").getLines().mkString)
    else {
      println(s"Error reading $id file")
      None
    }
  }  
}

case class MagicSet(
  block:Option[String],
  border:String,
  cards: List[MagicCard],  
  code:String,
  name:String,
  releaseDate:String,  
  `type`: String,
  gathererCode: Option[String],  
  booster: Option[JsArray]
)

case class MagicCard(
  name: String,
  manaCost: Option[String],
  cmc: Option[Int],
  colors: Option[List[String]],
  `type`: String,
  supertypes: Option[List[String]],
  types: List[String],
  subtypes: Option[List[String]],
  rarity: String,
  text: Option[String],
  flavor: Option[String],
  artist: String,
  number: Option[String],
  power: Option[String],
  toughness: Option[String],
  layout: String,
  multiverseid: Int,
  imageName: String,
  timeshifted:Option[Boolean]
)