package controllers;

import actors.PlayerAdmin;
import actors.messages.PlayerConnected;
import actors.messages.PlayerDisconnected;
import actors.messages.WSMessage;
import com.google.gson.Gson;
import akka.actor.ActorRef;
import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

    private static Gson gson = new Gson();

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static WebSocket<String> wsConnect() {
        return new WebSocket<String>() {
            @Override
            public void onReady(In<String> in, WebSocket.Out<String> out) {
                final String connectionId = java.util.UUID.randomUUID().toString();
                PlayerAdmin.ref().tell(new PlayerConnected(connectionId, out), ActorRef.noSender());

                in.onMessage(new Callback<String>() {
                    @Override
                    public void invoke(String event) {
                    	PlayerAdmin.ref().tell(new WSMessage(connectionId, event), ActorRef.noSender());
                    }
                });
                in.onClose(new Callback0() {
                    @Override
                    public void invoke() {
                    	PlayerAdmin.ref().tell(new PlayerDisconnected(connectionId), ActorRef.noSender());
                    }
                });
            }
        };
    }
    /*
     * public static WebSocket<JsonNode> wsConnect() {
     *
     * return new WebSocket<JsonNode>() {
     *
     * @Override public void onReady(In<JsonNode> in, Out<JsonNode> out) { //
     * use the out channel to push the data back to the client final String uuid
     * = java.util.UUID.randomUUID().toString();
     * WSConnectionManager.ref.tell(new NewConnectionEvent(uuid, out),
     * ActorRef.noSender());
     *
     * in.onMessage(new Callback<JsonNode>() {
     *
     * @Override public void invoke(JsonNode arg0) throws Throwable {
     * Logger.info("Recevied a msg"); Message value =
     * mapper.readValues(arg0.path("message"), Message.class);
     * Logger.info("Received Msg:" + value); } });
     *
     * in.onClose(new Callback0() {
     *
     * @Override public void invoke() throws Throwable {
     * WSConnectionManager.ref.tell(new CloseConnectionEvent(uuid),
     * ActorRef.noSender()); } }); } };
     *
     * }
     */
}
