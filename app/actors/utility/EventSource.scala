package actors.utility

import akka.actor.{Actor, ActorRef}
import play.Logger

object EventSource {
  case class RegisterListener(listener: ActorRef)
  case class UnregisterListener(listener: ActorRef)
  case object GetOnce
}

trait EventSourceTrait {
  def sendEvent[T](event: T): Unit
  def eventSourceReceive: Actor.Receive
}

trait EventSource extends EventSourceTrait { this: Actor =>
  import EventSource._

  var listeners = Vector.empty[ActorRef]

  def sendEvent[T](event: T): Unit = listeners foreach { _ ! event }

  def sendSingle(receiver: ActorRef): Unit

  def eventSourceReceive: Receive = {
    case RegisterListener(listener) => listeners =
      listeners :+ listener
      sendSingle(listener)
    case UnregisterListener(listener) => listeners = listeners filter { _ != listener }
    case GetOnce => sendSingle(sender)
  }
}