package actors

import akka.actor.{Props, ActorRef, Actor}
import actors.DraftQueueActor._
import models.{MagicCard, Magic}
import play.Logger
import actors.DraftQueueActor.EnqueuePack
import actors.messages._
import models.MagicCard
import actors.DraftQueueActor.ChosenDraftPick
import actors.DraftQueueActor.ChooseDraftPick
import scala.collection.immutable.HashMap
import actors.DraftQueueManager.StartPlaying
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import models.Converter

import scala.util.Random

object DraftQueueActor {
  case object StartQueue
  case object StartPicking
  case object BoosterEmpty
  case object ChangeDraftDirection
  case object StartDeckBuilding
  case class EnqueuePack(pack: List[MagicCard])
  case class AdjacentDrafters(left: ActorRef, right: ActorRef)
  case class ChooseDraftPick(cards: List[MagicCard])
  case class ChosenDraftPick(card: MagicCard)
  case class BuildDeckFromPicks(picks: List[MagicCard])
}

class Drafter(val player: ActorRef) extends Actor {
  private var leftAdjacent: ActorRef = _
  private var rightAdjacent: ActorRef = _

  private var picks = List[MagicCard]()
  private val packQueue = new scala.collection.mutable.Queue[List[MagicCard]]
  private var waitingForPick = false
  private var started = false
  private var passingLeft = true

  override def preStart() = {
    player ! StartQueue
  }

  def drafting: Receive = {
    case ChosenDraftPick(card) => pick(card)
    case EnqueuePack(pack) => enqueuePack(pack)
    case AdjacentDrafters(left, right) => setAdjacentDrafters(left, right)
    case StartPicking => start()
    case ChangeDraftDirection => passingLeft = !passingLeft
    case StartDeckBuilding => startDeckBuilding()
  }

  def deckBuilding: Receive = {
    case BuiltDraftDeck(deck, lands) => context.parent ! BuiltDraftDeckByPlayer(deck, lands, player)
  }

  def receive = drafting

  private def startDeckBuilding(): Unit = {
    context.become(deckBuilding)
    player ! BuildDeckFromPicks(picks)
  }

  private def setAdjacentDrafters(left: ActorRef, right: ActorRef): Unit = {
    leftAdjacent = left
    rightAdjacent = right
  }

  private def enqueuePack(pack: List[MagicCard]): Unit = {
    if(pack.size > 0) {
      packQueue.enqueue(pack)
    } else {
      context.parent ! BoosterEmpty
    }

    startPick()
  }

  def pick(card: MagicCard): Unit = {
    val booster = packQueue.dequeue()
    if(!booster.contains(card)) {
      Logger.error("Invalid Pick in booster: " + booster + " for card: " + card)
    }

    picks = card :: picks
    //Logger.info("We now have " + picks.size + " picks and the booster contains " + booster.size + " cards.")
    waitingForPick = false

    val remainingCards = booster diff List(card)
    if(passingLeft) {
      leftAdjacent ! EnqueuePack(remainingCards)
    } else {
      //Logger.info("passing to the right")
      rightAdjacent ! EnqueuePack(remainingCards)
    }

    startPick()
  }

  private def start(): Unit = {
    started = true
    startPick()
  }

  private def startPick(): Unit = {
    if(!started || waitingForPick || packQueue.isEmpty) {
      return
    }

    waitingForPick = true
    //Logger.info("sending the player the cards to pick from")
    player ! ChooseDraftPick(packQueue.head)
  }
}

class DraftQueueActor(q: DraftQueue, players: List[ActorRef]) extends Actor {
  //import context.dispatcher

  private var drafters: List[ActorRef] = _

  private var countFinishedPlayers = 0
  private var remainingBoosters = q.draftInformation.packs.drop(1)

  var decks: Map[ActorRef, (List[MagicCard], LandList)] = HashMap.empty

  def receive = {
    case StartQueue =>
      //send status message
      val draftingPlayers = players.map(p => context.actorOf(Props(new Drafter(p)))).zipWithIndex
      drafters = draftingPlayers.map {
        case (drafter, index) =>
          val leftIndex = if(index == 0) draftingPlayers.size-1 else index-1
          val rightIndex = if(index == draftingPlayers.size-1) 0 else index+1
          drafter ! AdjacentDrafters(draftingPlayers(leftIndex)._1, draftingPlayers(rightIndex)._1)
          drafter
      }

      dealBoosters(q.draftInformation.packs(0))
      drafters.foreach(_ ! StartPicking)

    case BoosterEmpty =>
      countFinishedPlayers += 1
      if(countFinishedPlayers == drafters.size) {
        countFinishedPlayers = 0
        remainingBoosters match {
          case x :: xs =>
            Logger.info("All players are finished with their current booster. Enqueueing the next one now.")
            remainingBoosters = xs
            drafters.foreach(_ ! ChangeDraftDirection)
            dealBoosters(x)
          case Nil =>
            Logger.info("All boosters have been drafted!")
            drafters.foreach(_ ! StartDeckBuilding)
        }
      }
    case BuiltDraftDeckByPlayer(deck, lands, player) =>
      decks = decks + (player -> (deck, lands))
      if (decks.size == players.size) {
        println("all players have built their deck")
        context.parent ! StartPlaying(decks)
      }
  }

  private def dealBoosters(pack: PackInfo): Unit = {
    println(s"Creating a booster for pack: $pack")
    getBoosters(pack).foreach { boosters  => 
      println("Enqueueing successfully created booster")
      drafters.zip(boosters).foreach {
        case (drafter, booster) => drafter ! EnqueuePack(booster)
      }
    }
    
    /*
    drafters.zip(getBoosters(pack)).foreach {
      case (drafter, booster) => drafter ! EnqueuePack(booster)
    } 
    */
  }

  private def getBoosters(pack: PackInfo): Option[Seq[List[MagicCard]]] = { //List[Option[List[MagicCard]]] = { //todo change return type
    val res = for (
      counter <- 1 to q.draftInformation.maxPlayers
    ) yield Converter.convert(pack)//Seq[Option[(PackInfo, Booster)]]
    
    ListHelper.sequence(res).map(_.map(_._2)) //Option[Seq[(PackInfo, Booster)]]

  }

  override def preStart() = {
    self ! StartQueue
  }
}

object ListHelper {
  def sequence[A](as: Seq[Option[A]]): Option[Seq[A]] = {
    if (as.contains(None))
      None
    else
      Some(as.map(_.get))
  }

  def sequenceL[A](as: List[Option[A]]): Option[List[A]] = {
    if (as.contains(None))
      None
    else
      Some(as.map(_.get))
  }  
}
