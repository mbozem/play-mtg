package actors

import akka.actor.{ActorRef, Actor}
import play.mvc.WebSocket.Out
import play.Logger
import actors.messages._
import actors.messages.UserStatus._
import actors.utility.EventSource.{GetOnce, RegisterListener}
import scala.Some
import actors.DraftQueueActor._
import scala.util.Random
import scala.Some
import actors.messages.DraftQueueJoined
import actors.messages.UserStatusMsg
import actors.DraftQueueActor.ChooseDraftPick
import actors.MatchActor.{MatchLost, MatchDrawn, MatchWon, StartGame}
import play.api.libs.json._
import play.api.libs.functional.syntax._
import models.EditionManager

class Player(draftQueueManager: ActorRef, playerId: String, originalOut: Out[String], initialStatus: String) extends Actor {

  var status = initialStatus
  var currentDraftId: Option[String] = None
  var draftQueueActor: ActorRef = _
  var matchActor: ActorRef = _
  var outChannel: Option[Out[String]] = Some(originalOut)
  var lastDraftPicksMsg: Option[DraftPicksMsg] = None

  def defaults: Receive = {
    case NewOutChannel(out) => reconnect(out)
    case PlayerLoggedOut => outChannel = None
    case StartGame =>
      Logger.debug("Player actor has become 'playing'.")
      matchActor = sender()
      status = PlayingMatch
      sendStatus()
      context.become(playing orElse defaults)
    case x: LoginSuccessMsg => send(x)
    case x: ConnectedUsersMsg =>
      println(s"Received ConnectedUsersMsg $x")
      send(x)
  }

  def notDrafting: Receive = {
    case m:CreateDraftQueueMsg => draftQueueManager ! (m, playerId)
    case m:JoinDraftQueueMsg => draftQueueManager ! (m, playerId)
    case m:LeaveDraftQueueMsg => draftQueueManager ! (m, playerId)
    case x:OutgoingClientMsg => send(x)
    case DraftQueueJoined(q) => joinQueue(q.draftId)
    case DraftQueueLeft => updateStatus(NotInDraft, None)
    case StartQueue =>
      draftQueueActor = sender()
      status = Drafting
      sendStatus()
      context.become(drafting orElse defaults)
  }

  def drafting: Receive = {
    case m: ChosenPick =>
      lastDraftPicksMsg = None
      draftQueueActor ! ChosenDraftPick(m.card)
    case m: BuiltDraftDeck => draftQueueActor ! m
    case ChooseDraftPick(cards) =>
      lastDraftPicksMsg = Some(DraftPicksMsg(cards))
      send(DraftPicksMsg(cards))
    case BuildDeckFromPicks(picks) =>
      status = DeckBuilding
      sendStatus()
      send(BuildDeckMsg(picks))
    case NewOutChannel(out) =>
      reconnect(out)
      resendDraftMsg()
  }

  def playing: Receive = {
    case x: OutgoingClientMsg => send(x)
    case x: DrawCardsMessage => matchActor ! x
    case x: PlayCardMessage => matchActor ! x
    case x: TapCardMessage => matchActor ! x
    case x: CardMovedMessage => matchActor ! x //todo diese messages zusammenfassen
    case x: ChangeLifeMessage => matchActor ! x
    case PlayerDeclaredResult(resStatus) =>
      resStatus match {
        case 1 => matchActor ! MatchWon
        case 0 => matchActor ! MatchDrawn
        case _ => matchActor ! MatchLost
      }
  }
  /*
  sealed trait MatchResultEntered
  case object MatchWon extends MatchResultEntered
  case object MatchDrawn extends MatchResultEntered
  case object MatchLost extends MatchResultEntered
   */

  def receive = notDrafting orElse defaults

  override def preStart() {
    draftQueueManager ! RegisterListener(self)
    sendStatus()
    sendAvailableDraftEditions()
  }

  private def joinQueue(draftId: String) = {
    updateStatus(WaitingInQueue, Some(draftId))
    Logger.info("Player joined a Queue!")
  }

  private def updateStatus(newStatus: String, draftId: Option[String]) = {
    status = newStatus
    currentDraftId = draftId
    sendStatus()
  }

  private def sendStatus() = {
    send(UserStatusMsg(status, currentDraftId))
  }

  private def sendAvailableDraftEditions() = {
    send(AvailableDraftEditionsMsg(editions = EditionManager.valid))
  }

  private def send(msg: OutgoingClientMsg) = {
    import actors.messages.IncomingReads._

    //outChannel.foreach(_.write(write(msg)))
    //todo json
    outChannel.foreach(_.write( Json.toJson(msg).toString() ))
  }

  def reconnect(out: Out[String]) = {
    outChannel = Some(out)
    draftQueueManager ! GetOnce //results in a dead letter when drafting
    sendStatus()
  }

  def resendDraftMsg() = {
    lastDraftPicksMsg match {
      case Some(x) =>
        Logger.debug("resendDraftMsg")
        send(x)
      case None =>
    }
  }


  //implicit val formats = Serialization.formats(NoTypeHints)

}