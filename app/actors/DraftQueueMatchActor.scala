package actors

import akka.actor.{Props, ActorRef, Actor}
import models.{Magic, MagicCard}
import actors.DraftQueueMatchActor.{MatchResult, StartMatches}
import actors.MatchActor._
import scala.collection.immutable.HashMap
import scala.util.Random
import actors.messages.{PlayCardMessage, DrawCardsMessage, OutgoingClientMsg}
import play.Logger
import actors.messages._

object DraftQueueMatchActor {
  case object StartMatches
  case class MatchResult(results: Map[ActorRef, Int])
}

class DraftQueueMatchActor(participants: Map[ActorRef, (List[MagicCard], LandList)]) extends Actor {
  private var playerPoints: List[(ActorRef, Int)] = List()
  private var playing: List[ActorRef] = List()

  def receive = {
    case StartMatches =>
      playing = pairPlayers()
    case MatchResult(results) =>
      playing = playing.filter(_ != sender())
      results.foreach { case (p, points) =>
         playerPoints.find(_._1 == p).foreach { case (_, prevPoints) =>
           playerPoints = playerPoints.filter(_._1 != p) :+ (p, prevPoints+points)
         }
      }
      println(s"There are still ${playing.size} players playing")
      println(s"Received Matchresults: $playerPoints")

      if (playing.isEmpty) {
        pairPlayers()
      }
  }

  override def preStart() = {
    //map and one assignment ?
    participants.keys.foreach { p =>
      playerPoints = (p, 0) :: playerPoints
    }
    playerPoints.sortBy(_._2)

    self ! StartMatches
  }

  def pairPlayers(): List[ActorRef] = {
    def pair(paired: List[ActorRef], unpaired: Map[ActorRef, (List[MagicCard], LandList)]): List[ActorRef] = {
      if (unpaired.size >= 2) {
        val matchActor = context.actorOf(Props(new MatchActor(unpaired.take(2))))
        pair(matchActor :: paired, unpaired.drop(2))
      } else {
        paired
      }
    }

    playerPoints = playerPoints.sortBy(_._2)
    val allPlayers = playerPoints.map { case (p, points) =>
      p -> participants(p)
    }
    pair(List(), allPlayers.toMap)
  }
}

object MatchActor {
  type Deck = List[MagicCard]
  case object StartGame
  //case class DrawCards(amount: Int)
  sealed trait MatchResultEntered
  case object MatchWon extends MatchResultEntered
  case object MatchDrawn extends MatchResultEntered
  case object MatchLost extends MatchResultEntered

}

class MatchActor(participants: Map[ActorRef, (Deck, LandList)]) extends Actor {

  var players: Map[ActorRef, MatchPlayer] = HashMap.empty
  var results: Map[ActorRef, MatchResultEntered] = Map()

  def receive = {
    case StartGame =>
      participants.keys.foreach(_ ! NewMatchStarting())
      startGame()
    case DrawCardsMessage(amount) => drawCards(amount)
    case PlayCardMessage(card) => playCard(card)
    case TapCardMessage(cardId, isEnemyCard) => tapCard(cardId, isEnemyCard)
    case x: CardMovedMessage => moveCard(x)
    case ChangeLifeMessage(amount) => changeLife(amount)
    case x: MatchResultEntered =>
      results.get(sender()) match {
        case Some(res) => println("Result entered twice, ignoring new result")
        case None =>
          println(s"Player entered Result: $x")
          results = results + (sender() -> x)

          val finishedPlayerCount = results.values.size
          if (finishedPlayerCount == participants.keys.size) {
            println("All players entered a result")
            val points = results.map { case (p, enteredResult) =>
              p -> (enteredResult match {
                case MatchWon => 3
                case MatchDrawn => 1
                case MatchLost => 0
              })
            }
            println(s"Sending MatchResult: ${MatchResult(points.toMap)}")
            context.parent ! MatchResult(points.toMap)
            context.stop(self)
          }
      }
  }

  override def preStart() = {
    /*
    participants.foreach { case (p, _) =>
      results = results + (p -> None)
    }
    */
    self ! StartGame
  }

  def startGame() = {
    Logger.debug("MatchActor is starting the game")
    participants.zipWithIndex.foreach {
      case ((actor, deck), index) =>
        val matchPlayer = new MatchPlayer(deck, index) //todo dont take index as id
        matchPlayer.drawCards(7)
        players = players + (actor -> matchPlayer)
        actor ! StartGame
    }
    sendMatchObject()
  }

  def drawCards(amount: Int) = {
    players(sender).drawCards(amount)
    sendMatchObject()
  }

  def playCard(card: MagicCard) = {
    players(sender).playCard(card)
    sendMatchObject()
  }

  def tapCard(cardId: String, isEnemyCard: Boolean) = {
    getPlayer(isEnemyCard).tapCard(cardId)
    sendMatchObject()
  }  

  def moveCard(msg: CardMovedMessage) = {
    getPlayer(msg.is_enemy_card).moveCard(msg)
    sendMatchObject()
  }

  private def getPlayer(enemy: Boolean): MatchPlayer = {
    if (enemy) {
      if (players.values.size > 2) {
        throw new Exception("this doesn't work if there are multiple opponents")
      }
      (players - sender()).values.head
    } else { 
      players(sender())
    }    
  }

  def changeLife(amount: Int) = {
    players(sender()).changeLife(amount)
    sendMatchObject()
  }

  def sendMatchObject() = {
    val othersState = players.map {
      case (key, value) => key -> value.toOtherMatchState
    }
    players.foreach {
      case (key, value) =>
        val othersMatchState = (othersState - key).map{ case (key2, value2) => value2 }
        key ! MatchObject(value.toOwnMatchState, othersMatchState)
    }
  }
}
class MatchPlayer(deckList: (Deck, LandList), playerId: Int) {

  private var deck: List[MagicCard] = Random.shuffle(deckList._1 ++ Magic.getLands(deckList._2))
  private var hand: List[MagicCard] = List()
  private var graveyard: List[MagicCard] = List()
  private var exiled: List[MagicCard] = List()
  private var inPlay: List[PlayedMagicCard] = List()
  private var life = 20

  def toOtherMatchState: OthersMatchState = {
    OthersMatchState(life, playerId, deck.size, hand.size, graveyard, exiled, inPlay)
  }

  def toOwnMatchState: OwnMatchState = {
    OwnMatchState(life, deck.size, hand, graveyard, exiled, inPlay)
  }

  def drawCards(amount: Int): Unit = {
    val drawnCards = for {
      i <- 0 until amount
      if deck.length >= i+1
    } yield deck(i)

    deck = deck.drop(drawnCards.length)
    hand = hand ++ drawnCards
  }

  def playCard(card: MagicCard): Unit = {
    if (!hand.contains(card)) Logger.error("Player played card that wasn't in his hand: " + card.name)

    hand = hand diff List(card)
    inPlay = inPlay :+ makeLiveCard(card)
  }

  def tapCard(cardId: String): Unit = {
    inPlay = inPlay.map { card =>
      if (card.id == cardId) {
        card.copy(position = card.position.copy(isTapped = !card.position.isTapped))
      } else {
        card
      }
    }
  }   

  def moveCard(msg: CardMovedMessage): Unit = {
    inPlay = inPlay.map { card =>
      if (card.id == msg.card_id) {
        //card.copy(position = CardPosition(msg.x, msg.y, Option(msg.zIndex)))
        card.copy(position = card.position.copy(x = msg.x, y = msg.y, zIndex = Option(msg.zIndex)))
      } else {
        card
      }
    }
  }

  def changeLife(amount: Int) = {
    life = life + amount
  }

  private def makeLiveCard(card: MagicCard): PlayedMagicCard = {
    PlayedMagicCard(getId, card, CardPosition(0, 0, None, false))
  }

  private def getId = java.util.UUID.randomUUID.toString

}
/*
case class OwnMatchState(deckSize: Int, hand: List[MagicCard], graveyard: List[MagicCard], exiled: List[MagicCard], inPlay: List[MagicCard])
case class OthersMatchState(playerId: Int, deckSize: Int, handSize: Int, graveyard: List[MagicCard], exiled: List[MagicCard], inPlay: List[MagicCard])

 */