package actors

import play.libs.Akka
import akka.actor.{Props, ActorRef, Actor}
import play.Logger
import actors.utility.EventSource
import actors.messages._
import actors.messages.DraftQueue
import scala.Some
import actors.messages.DraftInformation
import actors.messages.LeaveDraftQueueMsg
import actors.messages.DraftQueueJoined
import actors.messages.CreateDraftQueueMsg
import actors.messages.JoinDraftQueueMsg
import actors.messages.PlayerDisconnected
import actors.messages.DraftQueuesMsg
import akka.actor.Props
import models.MagicCard
import actors.DraftQueueManager.StartPlaying

object DraftQueueManager {
  lazy val ref: ActorRef = Akka.system.actorOf(Props(classOf[DraftQueueManager]))

  case class StartPlaying(participants: Map[ActorRef, (List[MagicCard], LandList)])
}

class DraftQueueManager extends Actor with EventSource {

  var queues: List[DraftQueue] = List()
  private var players:Map[String, List[ActorRef]] = Map()

  def receive = draftQueueReceive orElse eventSourceReceive

  def draftQueueReceive: Receive = {
    case (msg: CreateDraftQueueMsg, playerId: String) => 
      println(s"CreateDraftQueueMsg $msg")
      createQueue(msg.draftInformation, playerId, sender)
    case (msg: JoinDraftQueueMsg, playerId: String) => addPlayerToQueue(msg, playerId, sender)
    case (msg: LeaveDraftQueueMsg, playerId: String) => removePlayerFromQueues(playerId, sender)
    case PlayerDisconnected(playerId) =>
      removePlayerFromQueues(playerId, sender)
      //broadcasting queues is only necessary if the disconnected player has been in a queue
      broadCastQueues()
      Logger.info("Removed Player from queue, there are now " + queues.size + " queues. ")
    case StartPlaying(participants) =>
      //it might make sense not to send the actorrefs for the players in the participants map
      //and instead get them from the list of players
      //less passing around of actorrefs
      context.stop(sender)
      startPlaying(participants)
  }

  private def createQueue(draftInfo: DraftInformation, playerId: String, creator: ActorRef): Unit = {
   if (playerIsDrafting(playerId)) {
      Logger.info("Player attempted to create Queue although he is already in a draft! PlayerId: " + playerId)
      return
    }
    val draftId = getId
    val newQueue = DraftQueue(draftId, List(playerId), draftInfo)

    queues = newQueue :: queues
    players = players + (draftId -> List(creator))

    creator ! DraftQueueJoined(newQueue)
    broadCastQueues()

    Logger.info(" Created a new DraftQueue, there are now " + queues.size + " queues. ")
  }

  private def addPlayerToQueue(msg: JoinDraftQueueMsg, playerId: String, joiner: ActorRef): Unit = {
    if (playerIsDrafting(playerId)) {
      Logger.info("Player attempted to join Queue although he is already in a draft! PlayerId: " + playerId)
      return
    }

    if (queueIsFull(msg.draftId)) {
      Logger.info("Player attempted to join a queue that's already full! DraftId: " + msg.draftId)
      return
    }

    queues.find(q => q.draftId == msg.draftId).foreach { q =>
      val index = queues.indexOf(q)
      val newQueue = DraftQueue(q.draftId, playerId :: q.players, q.draftInformation)
      queues = queues.updated(index, newQueue)

      players.get(q.draftId).foreach(refs => players = players.updated(q.draftId, joiner :: refs))

      joiner ! DraftQueueJoined(newQueue)
      broadCastQueues()

      if(newQueue.players.size == newQueue.draftInformation.maxPlayers) {
        Logger.info("starting drafting in queue")
        startQueue(newQueue)
      }
    }
  }

  private def startQueue(q: DraftQueue) = {
    val queueActor = context.actorOf(Props(new DraftQueueActor(q, players.get(q.draftId).get)), q.draftId)
  }

  private def startPlaying(participants: Map[ActorRef, (List[MagicCard], LandList)]) = {
    val dqmActor = context.actorOf(Props(new DraftQueueMatchActor(participants)), "DraftQueueMatchActor")
  }

  private def broadCastQueues() = {
    sendEvent(DraftQueuesMsg(queues))
  }

  private def removePlayerFromQueues(removedPlayerId: String, origSender: ActorRef): Unit = {
    if(!playerIsDrafting(removedPlayerId)) return

    queues = queues.map {
      q => DraftQueue(q.draftId, q.players.filter(id => id != removedPlayerId), q.draftInformation)
    }
    players = players.map {
      case (draftId, actorRefs) => (draftId, actorRefs.filter(_ != origSender))
    }

    origSender ! DraftQueueLeft
    broadCastQueues()
    Logger.info("Removed player from draftQueue. " + removedPlayerId)
  }

  private def playerIsDrafting(playerId: String) = {
    queues.exists(draftQueue => draftQueue.players.exists(_ == playerId))
  }

  private def queueIsFull(draftId: String) = {
    queues.exists(q => q.draftId == draftId && q.players.length >= q.draftInformation.maxPlayers)
  }

  private def getId = {
    java.util.UUID.randomUUID.toString
  }

  override def sendSingle(receiver: ActorRef): Unit = {
    receiver ! DraftQueuesMsg(queues)
  }
}