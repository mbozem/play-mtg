package actors.messages

import akka.actor.ActorRef
import play.mvc.WebSocket.Out
import models.MagicCard
import julienrf.variants.Variants

//Utility classes

case class WSMessage(playerId: String, msg: String)

object UserStatus {
  val NotInDraft = "NOT_IN_DRAFT"
  val WaitingInQueue = "WAITING_IN_QUEUE"
  val Drafting = "DRAFTING"
  val DeckBuilding = "DECKBUILDING"
  val WaitingForMatch = "WAITING_FOR_MATCH"
  val PlayingMatch = "PLAYING_MATCH"
}
case class ConnectedUser(id: String, name: Option[String], status: String)

case class PackInfo(name: String, id: String)
case class DraftInformation(maxPlayers: Int, packs: List[PackInfo])
case class DraftQueue(draftId: String, players: List[String], draftInformation: DraftInformation)
case class DraftQueueJoined(queue: DraftQueue)
case object DraftQueueLeft

//Messages to the client:
sealed trait OutgoingClientMsg { val msgType: String }
case class UserStatusMsg(status: String, draftId: Option[String], msgType: String = "UserStatus") extends OutgoingClientMsg
case class ConnectedUsersMsg(users: Iterable[ConnectedUser], notLoggedInCount: Int, msgType: String = "ConnectedUsers") extends OutgoingClientMsg
case class DraftQueuesMsg(queues: List[DraftQueue], msgType: String = "DraftQueues") extends OutgoingClientMsg
case class DraftPicksMsg(picks: List[MagicCard], msgType: String = "DraftPicks") extends OutgoingClientMsg
case class BuildDeckMsg(cards: List[MagicCard], msgType: String = "BuildDeck") extends OutgoingClientMsg
case class AvailableDraftEditionsMsg(editions: List[PackInfo], msgType: String = "AvailableDraftEditions") extends OutgoingClientMsg

case class MatchObject(ownMatchState: OwnMatchState, otherPlayersState: Iterable[OthersMatchState], msgType: String = "MatchObject") extends OutgoingClientMsg

case class LoginSuccessMsg(username: String, msgType: String = "LoginMsg") extends OutgoingClientMsg
case class InvalidLoginMsg(reason: String, msgType: String = "InvalidLoginMsg") extends OutgoingClientMsg

case class NewMatchStarting(msgType: String = "NewMatchStartingMsg") extends OutgoingClientMsg

case class CardPosition(x: Double, y: Double, zIndex: Option[Int], isTapped: Boolean)
case class PlayedMagicCard(id: String, card: MagicCard, position: CardPosition)
case class OwnMatchState(life: Int, deckSize: Int, hand: List[MagicCard], graveyard: List[MagicCard], exiled: List[MagicCard], inPlay: List[PlayedMagicCard])
case class OthersMatchState(life: Int, playerId: Int, deckSize: Int, handSize: Int, graveyard: List[MagicCard], exiled: List[MagicCard], inPlay: List[PlayedMagicCard])

case class BuiltDraftDeckByPlayer(cards: List[MagicCard], lands: LandList, player: ActorRef)

sealed trait ReceivedMessage
case class CreateDraftQueueMsg(draftInformation: DraftInformation) extends ReceivedMessage
case class JoinDraftQueueMsg(draftId: String) extends ReceivedMessage
case class LeaveDraftQueueMsg(draftId: String) extends ReceivedMessage
case class ChosenPick(card: MagicCard) extends ReceivedMessage
case class BuiltDraftDeck(cards: List[MagicCard], lands: LandList) extends ReceivedMessage
case class LandList(plains: Int = 0, island: Int = 0, swamp: Int = 0, mountain: Int = 0, forest: Int = 0)
case class LoginMessage(username: String) extends ReceivedMessage
case class DrawCardsMessage(amount: Int) extends ReceivedMessage
case class PlayCardMessage(card: MagicCard) extends ReceivedMessage
case class TapCardMessage(card_id: String, is_enemy_card: Boolean) extends ReceivedMessage
case class ChangeLifeMessage(amount: Int) extends ReceivedMessage
case class PlayerDeclaredResult(result: Int) extends ReceivedMessage //0 = draw, 1 = win, -1 = loss
case class CardMovedMessage(card_id: String, is_enemy_card: Boolean, x: Double, y: Double, zIndex: Int) extends ReceivedMessage

case class IncomingMsg(message: ReceivedMessage)

object ReceiverReads {
  import play.api.libs.json._
  import IncomingReads._


  implicit val format: Format[ReceivedMessage] = Variants.format[ReceivedMessage]("theType")
  implicit val incReads = Json.reads[IncomingMsg]
}

object IncomingReads {
  import play.api.libs.json._
  import play.api.libs.functional.syntax._
  import models.MagicSet

  implicit val formats2 = Json.reads[CardPosition]
  implicit val formats4 = Json.writes[CardPosition]

  implicit val incReadsc = Json.reads[MagicCard]
  implicit val incReadsd = Json.reads[MagicSet]
  implicit val incReadsb = Json.reads[PackInfo]
  implicit val incReadsa = Json.reads[DraftInformation]
  implicit val incReadse = Json.reads[LandList]

  implicit val incReads1 = Json.reads[CreateDraftQueueMsg]
  implicit val incReads2 = Json.reads[JoinDraftQueueMsg]
  implicit val incReads3 = Json.reads[LeaveDraftQueueMsg]
  implicit val incReads4 = Json.reads[ChosenPick]
  implicit val incReads5 = Json.reads[BuiltDraftDeck]
  implicit val incReads6 = Json.reads[LoginMessage]
  implicit val incReads7 = Json.reads[DrawCardsMessage]
  implicit val incReads8 = Json.reads[PlayCardMessage]
  implicit val incReads9 = Json.reads[PlayerDeclaredResult]
  implicit val incReads10 = Json.reads[ChangeLifeMessage]
  implicit val incReads11 = Json.reads[NewMatchStarting]

  //implicit val incReads0 = Json.reads[IncomingClientMsg]

  implicit val incWrites11 = Json.writes[MagicCard]
  implicit val incWrites4 = Json.writes[PackInfo]
  implicit val incWrites3 = Json.writes[DraftInformation]
  implicit val incWrites1 = Json.writes[ConnectedUser]
  implicit val incWrites2 = Json.writes[DraftQueue]
  implicit val incWrites6 = Json.writes[LandList]

  implicit val formats1 = Json.reads[PlayedMagicCard]
  implicit val formats3 = Json.writes[PlayedMagicCard]

  implicit val incReadsconnUser = Json.reads[ConnectedUser]
  implicit val incReadsdraftQueue = Json.reads[DraftQueue]

  implicit val aa = Json.reads[OwnMatchState]
  implicit val aaa = Json.reads[OthersMatchState]

  implicit val bb = Json.writes[OwnMatchState]
  implicit val bbb = Json.writes[OthersMatchState]

/*
  implicit val outgoingformats: Writes[OutgoingClientMsg] = Writes {
    (msg: OutgoingClientMsg) => Json.obj(
      "msgType" -> msg.msgType
    )
  }
  */

/*
  implicit val incWrites6 = Json.writes[UserStatusMsg]
  implicit val incWrites7 = Json.writes[ConnectedUsersMsg]
  implicit val incWrites8 = Json.writes[DraftQueuesMsg]
  implicit val incWrites9 = Json.writes[DraftPicksMsg]
  implicit val incWrites10 = Json.writes[BuildDeckMsg]
  */
  //implicit val incWrites11 = Json.writes[OutgoingClientMsg]

  implicit val format: Format[OutgoingClientMsg] = Variants.format[OutgoingClientMsg]
}

//Actor Messages:
case class PlayerConnected(playerId: String, out: Out[String])
case class PlayerDisconnected(playerId: String)

case class NewOutChannel(out: Out[String])
case object PlayerLoggedOut