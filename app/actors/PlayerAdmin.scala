package actors

import scala.collection.immutable.HashMap
import akka.actor.Actor
import akka.actor.Props
import play.Logger
import play.libs.Akka
import play.mvc.WebSocket.Out
import akka.actor.ActorRef
import actors.messages._
import actors.messages.UserStatus._
import actors.messages.WSMessage
import actors.messages.PlayerConnected
import actors.PlayerAdmin.{Username, ConnectionId}
import models.{Magic, MagicCard}
import actors.MatchActor.Deck

import play.api.libs.json._
import play.api.libs.functional.syntax._

trait PlayerCreator {
  def newPlayer(playerId: String, out: Out[String], status: String): Actor = {
    new Player(DraftQueueManager.ref, playerId, out, status)
  }
}

object PlayerAdmin {
  type ConnectionId = String
  type Username = String
  lazy val ref: ActorRef = Akka.system.actorOf(Props(new PlayerAdmin with PlayerCreator), "PlayerAdmin")
}

class PlayerAdmin extends Actor {
  this: PlayerCreator =>

  var notLoggedInPlayers: Map[ConnectionId, Out[String]] = HashMap.empty
  var players: Map[Username, ActorRef] = HashMap.empty
  var connections: Map[ConnectionId, ActorRef] = HashMap.empty

  def receive = {
    case PlayerConnected(connectionId, out) =>
      createConnection(connectionId, out)
      broadCastPlayers()
    case PlayerDisconnected(playerId) =>
      disconnectPlayer(playerId)
      broadCastPlayers()
    case WSMessage(connectionId, msg) => forwardMessage(connectionId, msg)
    case x:OutgoingClientMsg => sendToAll(x)
  }

  private def broadCastPlayers(): Unit = {
    def findUserName(ref: ActorRef, search: List[(Username, ActorRef)]): Option[String] = {
      search match {
        case x :: xs => if (x._2 == ref) Some(x._1) else findUserName(ref, xs)
        case _ => None
      }
    }

    val playersWithName = players.map(p => p._1 -> p._2).toList
    val usernames = connections.map { case (id, ref) =>
      ConnectedUser(id, findUserName(ref, playersWithName), "")
    }
    val msg = ConnectedUsersMsg(usernames, notLoggedInPlayers.size)
    sendToAll(msg)
    sendToNotLoggedInPlayers(msg)
  }

  private def sendToNotLoggedInPlayers(msg: OutgoingClientMsg) = {
    import actors.messages.IncomingReads._
    notLoggedInPlayers.values.foreach { out =>
      out.write( Json.toJson(msg).toString())
    }
  }

  private def logInPlayer(connectionId: ConnectionId, username: Username): Unit = {
    if (connections.get(connectionId).isDefined) return

    if (username == "fail")  {
      import actors.messages.IncomingReads._
      val invalidLoginMsg = InvalidLoginMsg("du stinkst")
      notLoggedInPlayers(connectionId).write(Json.toJson(invalidLoginMsg).toString())
      return
    }

    players.get(username) match {
      case Some(player) =>
        player ! NewOutChannel(notLoggedInPlayers(connectionId))
        connections = connections + (connectionId -> player)
        player ! LoginSuccessMsg(username)
        Logger.info(s"Player($username) reconnected.")
      case None =>
        val playerId = getId
        val outChannel = notLoggedInPlayers(connectionId)
        val player = context.actorOf(Props(newPlayer(playerId, outChannel, NotInDraft)), playerId)
        players = players + (username -> player)
        notLoggedInPlayers = notLoggedInPlayers - connectionId
        connections = connections + (connectionId -> player)
        player ! LoginSuccessMsg(username)
        Logger.info(s"New Player($username) logged in. There are now ${players.size} players.")


        // zum testen von games um nicht erst komplett draften zu müssen
        if (players.size == 2) {
          //startTestGame()
        }

    }
    broadCastPlayers()
  }

  private def createConnection(connectionId: ConnectionId, out: Out[String]) = {
    notLoggedInPlayers = notLoggedInPlayers + (connectionId -> out)
  }

  private def disconnectPlayer(playerId: String) = {
    notLoggedInPlayers = notLoggedInPlayers - playerId
    connections.get(playerId).foreach(_ ! PlayerLoggedOut)
    connections = connections - playerId
    Logger.info("Player disconnected")
  }

  private def forwardMessage(connectionId: ConnectionId, msg: String) = {

    def forward(m: AnyRef): Unit = {
      connections.get(connectionId).foreach(_ ! m)
    }

      import actors.messages.IncomingReads._

    import actors.messages.ReceiverReads._
    Json.parse(msg).validate[IncomingMsg] match {
      case s: JsSuccess[IncomingMsg] =>
        s.get.message match {
          case LoginMessage(username) => logInPlayer(connectionId, username)
          case x => forward(x)
        }
      case e: JsError =>
        println(s"Error: ${e.errors}")
        println(msg)
    }
  }

  def startTestGame() = {
    val participants: Map[ActorRef, (Deck, LandList)] = players.map {
      case (username, actor) => actor -> Magic.makeRandomDeck()
    }
    context.actorOf(Props(new MatchActor(participants)), "MatchActor")

  }

  private def sendToAll(msg: AnyRef) = {
    connections.values.foreach(_ ! msg)
  }

  private def getId = {
    java.util.UUID.randomUUID.toString
  }

}